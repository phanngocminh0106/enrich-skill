const express = require("express");
const router = express.Router();
const csrf = require("csurf");
var passport = require("passport");
const middleware = require("../middleware");
const {
  userSignUpValidationRules,
  userSignInValidationRules,
  validateSignup,
  validateSignin,
} = require("../config/validator");
const csrfProtection = csrf();
router.use(csrfProtection);
const {
  renderSignUpFormService,
  handleSignUpService,
  renderSignInService,
  handleSignInService,
  displayProfileService,
  logOutService,
} = require("../services/user.service");

// GET: display the signup form with csrf token
router.get("/signup", middleware.isNotLoggedIn, (req, res) => {
  renderSignUpFormService(req, res);
});
// POST: handle the signup logic
router.post(
  "/signup",
  [
    middleware.isNotLoggedIn,
    userSignUpValidationRules(),
    validateSignup,
    passport.authenticate("local.signup", {
      failureRedirect: "/user/signup",
      failureFlash: true,
    }),
  ],
  async (req, res) => {
    
    handleSignUpService(req, res);
  }
);

// GET: display the signin form with csrf token
router.get("/signin", middleware.isNotLoggedIn, async (req, res) => {
  renderSignInService(req, res);
});

// POST: handle the signin logic
router.post(
  "/signin",
  [
    middleware.isNotLoggedIn,
    userSignInValidationRules(),
    validateSignin,
    passport.authenticate("local.signin", {
      failureRedirect: "/user/signin",
      failureFlash: true,
    }),
  ],
  async (req, res) => {
    handleSignInService(req, res);
  }
);

// GET: display user's profile
router.get("/profile", middleware.isLoggedIn, async (req, res) => {
  displayProfileService(req, res);
});

// GET: logout
router.get("/logout", middleware.isLoggedIn, (req, res) => {
  logOutService(req, res);
});
module.exports = router;
