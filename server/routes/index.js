const express = require("express");
const csrf = require("csurf");
const middleware = require("../middleware");
const router = express.Router();
const {
  renderHomePageService,
  addProductsService,
  renderShoppingContentService,
  reducerItemService,
  removeAllItemService,
  checkOutService,
} = require("../services/index.service");

const csrfProtection = csrf();
router.use(csrfProtection);

// GET: home page
router.get("/", (req, res) => {
  renderHomePageService(req, res);
});

// GET: add a product to the shopping cart when "Add to cart" button is pressed
router.get("/add-to-cart/:id", (req, res) => {
  addProductsService(req, res);
});

// GET: view shopping cart contents
router.get("/shopping-cart", (req, res) => {
  renderShoppingContentService(req, res);
});

// GET: reduce one from an item in the shopping cart
router.get("/reduce/:id", (req, res, next) => {
  // if a user is logged in, reduce from the user's cart and save
  // else reduce from the session's cart
  reducerItemService(req, res);
});

// GET: remove all instances of a single product from the cart
router.get("/removeAll/:id", (req, res, next) => {
  removeAllItemService(req, res);
});

// POST: handle checkout logic and payment using Stripe
router.post("/checkout", middleware.isLoggedIn, (req, res) => {
  checkOutService(req, res);
});

module.exports = router;
