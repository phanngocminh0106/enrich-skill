const express = require("express");
const router = express.Router();
const csrf = require("csurf");
const Order = require("../models/order");
const Cart = require("../models/cart");
const sendMail = require("../nodemailer");
const csrfProtection = csrf();
router.use(csrfProtection);
const renderSignUpFormService = async (req, res) => {
  var errorMsg = req.flash("error")[0];
  res.render("user/signup", {
    csrfToken: req.csrfToken(),
    errorMsg,
    pageName: "Sign Up",
  });
};

const handleSignUpService = async (req, res) => {
  try {
    if(req.body.email){
      sendMail(req)
    }
    if (req.session.cart) {
      const cart = await new Cart(req.session.cart);
      cart.user = req.user._id;
      await cart.save();
    }
    if (req.session.oldUrl) {
      var oldUrl = req.session.oldUrl;
      req.session.oldUrl = null;
      res.redirect(oldUrl);
    } else {
      res.redirect("/user/profile");
    }
  } catch (err) {
    console.log(err);
    req.flash("error", err.message);
    return res.redirect("/");
  }
};

const renderSignInService = (req, res) => {
  var errorMsg = req.flash("error")[0];
  res.render("user/signin", {
    csrfToken: req.csrfToken(),
    errorMsg,
    pageName: "Sign In",
  });
};

const handleSignInService = async (req, res) => {
  try {
    // cart logic when the user logs in
    let cart = await Cart.findOne({ user: req.user._id });
    // if there is a cart session and user has no cart, save it to the user's cart in db
    if (req.session.cart && !cart) {
      const cart = await new Cart(req.session.cart);
      cart.user = req.user._id;
      await cart.save();
    }
    // if user has a cart in db, load it to session
    if (cart) {
      req.session.cart = cart;
    }
    // redirect to old URL before signing in
    if (req.session.oldUrl) {
      var oldUrl = req.session.oldUrl;
      req.session.oldUrl = null;
      res.redirect(oldUrl);
    } else {
      res.redirect("/user/profile");
    }
  } catch (err) {
    console.log(err);
    req.flash("error", err.message);
    return res.redirect("/");
  }
};

const displayProfileService = async (req, res) => {
  const successMsg = req.flash("success")[0];
  const errorMsg = req.flash("error")[0];
  try {
    // find all orders of this user
    allOrders = await Order.find({ user: req.user });
    res.render("user/profile", {
      orders: allOrders,
      errorMsg,
      successMsg,
      pageName: "User Profile",
    });
  } catch (err) {
    console.log(err);
    return res.redirect("/");
  }
};

const logOutService = (req, res) => {
  req.logout();
  req.session.cart = null;
  res.redirect("/");
};

module.exports = {
  renderSignUpFormService,
  handleSignUpService,
  renderSignInService,
  handleSignInService,
  displayProfileService,
  logOutService,
};
