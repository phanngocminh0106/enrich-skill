const nodemailer = require("nodemailer");
const transporter = nodemailer.createTransport({
  host: 'smtp.gmail.com',
  port: 587,
  secure: false,
  auth: {
    user: process.env.GMAIL_EMAIL,
    pass: process.env.GMAIL_PASSWORD,
  },
  tls: {
    rejectUnauthorized: false,
  },
});

const sendMail = (req) => {
  return transporter.sendMail({
    from: process.env.GMAIL_EMAIL,
    to: req.body.email,
    subject: `SignUp Successfully`,
    html: `
    You have signUp successfully
      `,
  });
};
module.exports = sendMail;
