const express = require("express");
const router = express.Router();
const {
  getAllProductService,
  searchProductService,
  getCategory,
  getCertainProduct,
} = require("../services/products.service");
// GET: display all products
router.get("/", async (req, res) => {
  getAllProductService(req, res);
});

// GET: search box
router.get("/search", async (req, res) => {
  searchProductService(req, res);
});

//GET: get a certain category by its slug (this is used for the categories navbar)
router.get("/:slug", async (req, res) => {
  getCategory(req, res);
});

// GET: display a certain product by its id
router.get("/:slug/:id", async (req, res) => {
  getCertainProduct(req, res);
});

module.exports = router;
